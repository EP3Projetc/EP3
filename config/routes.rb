Rails.application.routes.draw do
  resources :livros
  devise_for :users, :controllers => { registrations:'registrationscontroller' }
  get 'home/index'
  root 'livros#index'
  resources :'help'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
