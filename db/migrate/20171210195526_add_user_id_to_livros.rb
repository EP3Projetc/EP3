class AddUserIdToLivros < ActiveRecord::Migration[5.1]
  def change
     add_column :livros, :user_id, :integer
  end
end
