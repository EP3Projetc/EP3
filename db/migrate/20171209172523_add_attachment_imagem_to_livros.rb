class AddAttachmentImagemToLivros < ActiveRecord::Migration[4.2]
  def self.up
    change_table :livros do |t|
      t.attachment :imagem
    end
  end

  def self.down
    remove_attachment :livros, :imagem
  end
end
