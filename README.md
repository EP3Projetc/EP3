<h1> Biblioteca Virtual </h1>
<h2>Motivadores para a ideia:<h2>
<p> Como podemos observar, hoje em dia cada vez mais a população vem deixando de consumir livros físicos da mesma forma com que consumia antigamente. Isso gerou em nós do grupo um enorme desconforto quando analisamos algumas empresas que realmente estão deixando de lado a parte física da leitura. Por conta disso, para atender o mercado ainda grande de pessoas que gostam dos livros físicos, decidimos fazer um site com o foco de anunciar esses livros de forma fácil  sem ter que nos preocupar com a parte de estocar esses livros ou vender, pois seremos apenas o intermédio de quem quer comprar ou vender o livro.</p>
<img src= "app/assets/images/Lampada.jpg">

<h2>Inspirações</h2>
<p>Nossas inspirações vinheram após as aulas de Gestão da Produção e Qualidade, em que nos foi mostrados diversos conceitos, tais como a de economia compartilhada, e exemplos como o do Uber ,OLX, Ifood.</p>
<p>Esses exemplos foram cruciais para que trilhacemos esse caminho de fazer um WebApp que os detentores dos objetos em destaque, fazendo com que não tenhamos que nos preocupar com os diretos em cima da compra ou troca de produtos.</p>
<img src="app/assets/images/economia_compartilhada.jpg">

<h2>Explicações a respeito do EP:<h2>
<p> Nosso EP foi feito na linguagem Ruby com o framework Rails. Usamos as gems paperclip, devise, scaffold, railsroad, bootstrap,jquery-rails, além das quem vem com o padrão de criação do projeto pelo código rails new "Nome do Projeto".</p>
<p> O projeto vem no modelo MVC (Model, View, Controller), que é muito explorado pela linguagem, sendo que tudo que você fará no projeto passará por umas dessas três partes.</p>
<p>Nossa página inicial consiste na amostra de todos os livros do banco de dados em formato de conteiner do bootstrap com uma breve apresentação do livro como título, autor, ano, edição e um botão de "Read More" que ao ser pressionado, caso você não esteja logado, irá te levar para a tela de login, onde também pode ser efetuado o cadastro de usuário.</p> 
<p> Infelizmente, nas últimas atualizações do app a função de resgistrar deu um problema, em que não está gerando erro, porém também não está resgistrando novos úsuarios. Então pedimos que entre com o login:victorhdcoelho@gmail.com e senha: 123456. Após logado você irá ser redirecionado para a página dos livros novamente, onde irá aparecer na barra superior novas opções que são Log out e new book.</p>
<p> Se clicar em new book irá criar um novo livro com o usuário que está logado, gravando assim o ID dele junto ao bando de dados de Livros(Relação feita no trabalho), em que muitos livros podem pertencer a um usuário, mas muitos usuários não podem ter o mesmo livro. Após logado também podera ver a descrição do produto depois de apertar o READ MORE do livro.</p>
<p> Apenas pessoal logado pode editar e deletar um livro, caso contrário, nem aparecerão as opções para isso. Na descrição estará descrimidado preço ou troca, estado do livro,entre outros assuntos pertinentes.</p>
<p>home</p>
<img src="app/assets/images/Pagina_home.png">
<p>Log in<p>
<img src="app/assets/images/Pagina_Login.png">
<p>Sing Up</p>
<img src="app/assets/images/Pagina_SingUp.png">
<p>Exemplo</p>
<img src="app/assets/images/Pagina_de_exemplo.png">

<h2>Para rodar a página:<h2>
<p>Primeiramente dê clone na pasta e coloque em algum diretório. Em segundo lugar, abra o terminal na pasta em que foi deixado o EP3. Em terceiro lugar, dê um bundle install para se certificar que todos os pacotes estão instalados. Quarto lugar, dê rails s e abra o navegador na seguinte página "http://localhost:3000/"</p>
<p>Pronto, agora é só ver os livros que você mais gosta, ou trocar aquele que você já tem há algum tempo!<p>
<p>O diagrama de classes está na pasta doc do projeto!<p>
